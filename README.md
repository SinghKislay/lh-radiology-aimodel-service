# LibreHealth Radiology Artificial Intelligence Model Service

[![pipeline status](https://gitlab.com/librehealth/radiology/lh-radiology-aimodel-service/badges/master/pipeline.svg)](https://gitlab.com/librehealth/radiology/lh-radiology-aimodel-service/-/commits/master)

The LH Radiology AI Model Service is a web service that allows communication with the different AI models. The [LibreHealth RIS](https://gitlab.com/librehealth/radiology/lh-radiology) uses this service to perform two-way communication (i.e. training and inference) between different types of AI models for radiology. The outputs of the models are meant to be shown in the DICOM Viewer embedded with lh-radiology, but may also be used by other parts of the system such as prioritizing a study list, assigning a specific type of study list to a specific radiologist user or scheduling different modalities for the PACS to capture, etc. The Service provides a generalized set of endpoints for the following:
 * Getting a list of supported models and their features.
 * Getting a list of pathologies that exist in an image (image classification) and their likely occurrance (probabilities).
 * Getting bounding boxes for regions of interest for labels in radiology images (image localization).
 * Getting segments in the regions of interest for labels in radiology images (image segmentation).
 * Training/Retraining a model with labels, bounding boxes and segments drawn on radiology images by POST to these endpoints.
 
The full spec for the AI model service can be found at: https://librehealth.gitlab.io/radiology/lh-radiology-aimodel-service/
