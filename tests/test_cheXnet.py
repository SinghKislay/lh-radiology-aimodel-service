import pytest
import requests
import json


url = "http://127.0.0.1:5000/cheXnet"

def test_post() :
    data = { "image": open("data/1_1.dcm", "rb") }
    response = requests.post(url , files = data)
    
    data = response.json()
    print(json.dumps(data, indent = 3))
    
    assert response.status_code == 200
    assert data['status'] == 'Success'