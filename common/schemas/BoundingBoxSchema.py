from marshmallow import Schema, fields


class BoundingBoxSchema(Schema):
    disease = fields.String(description="Detected disease")
    x1 = fields.Number()
    y1 = fields.Number()
    x2 = fields.Number()
    y2 = fields.Number()
    x3 = fields.Number()
    y3 = fields.Number()
    x4 = fields.Number()
    y4 = fields.Number()
