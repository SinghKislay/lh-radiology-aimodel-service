from flask import Flask
from flask_restful import Api
from apispec import APISpec

from apispec.ext.marshmallow import MarshmallowPlugin
from flask_apispec.extension import FlaskApiSpec

from resources.aimodel import aimodel
from resources.cheXnet import cheXnet
from resources.updateCheXnet import updateCheXnet

app = Flask(__name__)
api = Api(app)

app.config.update({
    'APISPEC_SPEC': APISpec(
        title='AI Model Service',
        version='v1',
        plugins=[MarshmallowPlugin()],
        openapi_version='2.0'
    ),
    'APISPEC_SWAGGER_URL': '/swagger/',  # URI to access API Doc JSON
    'APISPEC_SWAGGER_UI_URL': '/swagger-ui/'  # URI to access UI of API Doc
})

docs = FlaskApiSpec(app)

api.add_resource(aimodel, '/models')
docs.register(aimodel)

api.add_resource(cheXnet, '/bounding-box')
docs.register(cheXnet)

api.add_resource(updateCheXnet, '/model-update')
docs.register(updateCheXnet)

if __name__ == '__main__':
    app.run(debug=True)
