import werkzeug
import os
from flask_apispec import doc, marshal_with, use_kwargs
from flask import current_app as app
from flask_apispec.views import MethodResource
from flask_restful import Resource, reqparse
from marshmallow import Schema, fields
from common.schemas.BoundingBoxSchema import BoundingBoxSchema
from common.schemas.ModelListSchema import ModelListSchema
from model.cheXnet import getcoordinates

from datetime import datetime
from uuid import uuid4

parser = reqparse.RequestParser()
parser.add_argument('image', type=werkzeug.datastructures.FileStorage, location='files')


class BoundingBoxRequestSchema(Schema):
    image = fields.Raw(description="DICOM Image", required=True)
    model = fields.String(description="Model Name", required=True)
    modelVersion = fields.String(description="Model Version", required=True)


class BoundingBoxResponseSchema(Schema):
    status = fields.String(default='Success', required=True)
    model = fields.String(required=True)
    data = fields.List(fields.Nested(BoundingBoxSchema))


class cheXnet(MethodResource, Resource):
    @doc(summary="Get Bounding Box Models", description='Returns the the list of Bounding Box models with the model versions', tags=['Bounding Box'])
    @marshal_with(ModelListSchema)
    def get(self):
        return {"status": "Success", "data": [{"model: chexNet, version: 1.0"}]}

    @doc(summary="Get Disease and Bounding Boxes for DICOM", description='Returns the Bounding Boxes for the DICOM Image with the detected disease and the coordinates', tags=['Bounding Box'])
    @use_kwargs(BoundingBoxRequestSchema)
    @marshal_with(BoundingBoxResponseSchema)
    def post(self):
        args = parser.parse_args()
        image_file = args['image']

        filename = werkzeug.utils.secure_filename(image_file.filename)
        app.logger.debug("Recieved image - " + filename)

        app.logger.debug("Creating temp directory if it doesn't exist")
        os.makedirs("temp", exist_ok=True)

        # Handle multiple POST requests with the same filename
        # at the exact same time
        unique_id = datetime.now().strftime('%Y%m-%d%H-%M%S-') + str(uuid4())
        filename = unique_id + "_" + filename

        file_path = "temp/" + filename
        app.logger.debug("Saving file to disk - " + file_path)
        image_file.save(file_path)
        app.logger.debug("Saved file to disk - " + file_path)

        data = getcoordinates(file_path)

        # Cleanup: Delete Files
        if os.path.exists(file_path):
            os.remove(file_path)

        return {'status': 'Success', 'model': 'CheXnet', 'data': data}
