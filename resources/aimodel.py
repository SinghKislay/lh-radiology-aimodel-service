from flask_apispec import doc, marshal_with
from flask_apispec.views import MethodResource
from flask_restful import Resource
from common.schemas.ModelListSchema import ModelListSchema

class aimodel(MethodResource, Resource):
    @doc(summary="Get all Models", description='Returns the list of all available models in the system', tags=['Models'])
    @marshal_with(ModelListSchema)
    def get(self):
        return {"status": "Success", "data": [{"model: chexNet, version: 1.0"}]}